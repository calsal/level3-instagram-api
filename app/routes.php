<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Index page
|--------------------------------------------------------------------------
|
*/
Route::get('/', 'HomeController@index');

/*
|--------------------------------------------------------------------------
| Authorization redirect URI
|--------------------------------------------------------------------------
|
*/

Route::get('redirect', function() 
{
    // Get the oAuthToken
    $data = Instagram::getOAuthToken(Input::get('code'));
    // Set the access token
    Instagram::setAccessToken($data->access_token);
    Session::put('authenticated_user', $data->user->id);
    // Encrypt the access token with AES
    $encrypted = Crypt::encrypt($data->access_token);
	// Store the encrypted access token in DB
	$user = User::firstOrCreate(array('id' => '1'));
	$user->access_token = $encrypted;
	$user->save();

    // Redirect to authorized view        
    return View::make('authorized');
});

/*
|--------------------------------------------------------------------------
| User Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/users/search/', array('as' => 'search', 'uses' => 'UsersController@getSearch')); 
Route::get('/users/{id}/', array('as' => 'getUser', 'uses' => 'UsersController@getUser'));  
Route::get('/users/{id}/media/recent/', array('as' => 'recent', 'uses' => 'UsersController@getMediaRecent')); 
Route::get('/users/self/media/liked/', array('as' => 'liked', 'uses' => 'UsersController@getLiked'));
Route::get('/users/self/feed/', array('as' => 'feed', 'uses' => 'UsersController@getSelfFeed')); 
Route::get('/users/{id}/followed-by', array('as' => 'followed-by', 'uses' => 'UsersController@getFollowedBy'));
Route::get('/users/{id}/follows', array('as' => 'follows', 'uses' => 'UsersController@getFollows'));
Route::post('/users/{id}/follow/', array('as' => 'follow', 'uses' => 'UsersController@postFollow')); 
Route::delete('/users/{id}/unfollow/', array('as' => 'unfollow', 'uses' => 'UsersController@deleteUnfollow'));

/*
|--------------------------------------------------------------------------
| Media Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/media/search', array('as' => 'searchMedia', 'uses' => 'MediaController@searchMedia'));
Route::get('/media/popular', array('as' => 'getPopular', 'uses' => 'MediaController@getPopular'));
Route::get('/media/{id}', array('as' => 'getMedia', 'uses' => 'MediaController@getMedia'));
Route::put('/media/{id}/likes', array('as' => 'likeMedia', 'uses' => 'MediaController@likeMedia'));
Route::get('/media/{id}/likes', array('as' => 'getMediaLikes', 'uses' => 'MediaController@getMediaLikes'));
Route::delete('/media/{id}/likes', array('as' => 'deleteLikedMedia', 'uses' => 'MediaController@deleteLikedMedia'));

/*
|--------------------------------------------------------------------------
| Comment Routes
|--------------------------------------------------------------------------
|
*/

Route::post('/media/{id}/comments', array('as' => 'addMediaComment', 'uses' => 'CommentController@getMediaComments'));
Route::get('/media/{id}/comments', array('as' => 'getMediaComments', 'uses' => 'CommentController@getMediaComments'));
Route::delete('/media/{id}/{commentID}', array('as' => 'deleteMediaComment', 'uses' => 'CommentController@deleteMediaComment'));

/*
|--------------------------------------------------------------------------
| Location Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/locations/search', array('as' => 'getLocationSearch', 'uses' => 'LocationController@getlocationSearch'));
Route::get('/locations/{id}', array('as' => 'getLocation', 'uses' => 'LocationController@getLocation'));
Route::get('/locations/{id}/media/recent', array('as' => 'getLocationMedia', 'uses' => 'LocationController@getMediaForLocation'));
