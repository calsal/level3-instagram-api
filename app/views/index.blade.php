@extends('master')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
        	<img src="img/logo.png" height="256" width="256">
        	<h3>Developed for Web Services</h3>
        	<h5>Gerrit Eedema, Arya Ghosi, Carl Salmonsson, Christian Ragnarsson </h5>
        	<a href="https://bitbucket.org/cragnarsson/level-3-instagram-api" class="btn btn-primary btn-lg" role="button">Source code</a>
        	<a href="http://damp-sierra-3292.herokuapp.com/dist/index.html" class="btn btn-primary btn-lg" role="button">Documentation</a>
			<a href="{{$loginUrl}}" class="btn btn-primary btn-lg" role="button">Login with Instagram</a>
        </div>
    </div>
</div>

@stop
