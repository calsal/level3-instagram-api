<?php

include 'helpers/LevelThreeGenerators.php';

class CommentController extends BaseController {

/*
|--------------------------------------------------------------------------
| POST /comment/{id}/{commentID} - Post a comment by id of media and commentID.
| Parameters : id - The id of the media.
|             commentID - The id of the comment
|--------------------------------------------------------------------------
|
*/
    public function addMediaComment($id,$text)
    {
        Instagram::setAccessToken(User::getAccessToken());
        $mediaComment = Instagram::addMediaComment($id, $text);

        return Response::make(json_encode($result, JSON_PRETTY_PRINT), $result->meta->code);
    }

/*
|--------------------------------------------------------------------------
| GET /comment/{id} - Get all comments for a specific media.
| Parameter : id - The id of the media.
|--------------------------------------------------------------------------
|
*/
    public function getMediaComments($id)
    {
        Instagram::setAccessToken(User::getAccessToken());
        $mediaComments = Instagram::getMediaComments($id);

        return LevelThreeGenerators::generateLevelThreeSimple($mediaComments, false);
    }
/*
|--------------------------------------------------------------------------
| DELETE /comment/{id}/{commentID} - Delete a comment by id of media and commentID.
| Parameters : id - The id of the media.
|             commentID - The id of the comment
|--------------------------------------------------------------------------
|
*/
    public function deleteMediaComment($id,$commentID)
    {
        Instagram::setAccessToken(User::getAccessToken());
        $deleteMediaComment = Instagram::deleteMediaComment($id, $commentID);

        return Response::make(json_encode($result, JSON_PRETTY_PRINT), $result->meta->code);
    }
}