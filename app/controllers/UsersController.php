<?php

include 'helpers/LevelThreeGenerators.php';

class UsersController extends BaseController{


/*
|--------------------------------------------------------------------------
| GET /users/search - Search for a user by name. 
| Parameter : q - The string to search for.
|--------------------------------------------------------------------------
|
*/
    public function getSearch()
    {   
        if(Input::has('q')) {
            Instagram::setAccessToken(User::getAccessToken());
            $search = Instagram::searchUser(Input::get('q'));
            
            return LevelThreeGenerators::generateLevelThreeSimple($search, false);
        } else {
            App::abort(403, 'Necessary parameter not included in request(q).');
        }

    }
/*
|--------------------------------------------------------------------------
| GET /users/user-id - Get basic information about a user.
| Parameter : $id - The id of the user.
|--------------------------------------------------------------------------
|
*/
    public function getUser($id)
    {
        Instagram::setAccessToken(User::getAccessToken());
        $user = Instagram::getUser($id);

        return LevelThreeGenerators::generateLevelThreeSimple($user,false);

    }
/*
|--------------------------------------------------------------------------
| GET /users/self/feed - See the authenticated user's feed.
|--------------------------------------------------------------------------
|
*/
    public function getSelfFeed()
    {   
        Instagram::setAccessToken(User::getAccessToken());
        $feed = Instagram::getUserFeed();

        return LevelThreeGenerators::generateLevelThreeAdvanced($feed);

    }

/*
|--------------------------------------------------------------------------
| GET /users/{id}/followed-by - Get the list of users this user is followed by.
| Parameter : $id - The id of the user.
|--------------------------------------------------------------------------
|
*/
    public function getFollowedBy($id) {
        Instagram::setAccessToken(User::getAccessToken());
        $followers = Instagram::getUserFollower($id, Input::get('limit'));


       return LevelThreeGenerators::generateLevelThreeSimple($followers, false);

    }

/*
|--------------------------------------------------------------------------
| GET /users/{id}/follows - Get the list of users this user follows.
| Parameter : $id - The id of the user.
|--------------------------------------------------------------------------
|
*/
    public function getFollows($id) {
        Instagram::setAccessToken(User::getAccessToken());
        $follows = Instagram::getUserFollows($id, Input::get('limit'));


        return LevelThreeGenerators::generateLevelThreeSimple($follows, false);

    }
/*
|--------------------------------------------------------------------------
| GET /users/{id}/media/recent - Get the most recent media published by a user.
| Parameter : $id - The id of the user.
|--------------------------------------------------------------------------
|
*/
    public function getMediaRecent($id)
    {   
        Instagram::setAccessToken(User::getAccessToken());
        if(Input::has('count')) {
            $media = Instagram::getUserMedia($id, Input::get('count'));
        } else {
            $media = Instagram::getUserMedia($id);
        }
        

        return LevelThreeGenerators::generateLevelThreeAdvanced($media);

    }
/*
|--------------------------------------------------------------------------
| GET /users/self/media/liked - See the authenticated user's list of media they've liked. 
|--------------------------------------------------------------------------
|
*/
    public function getLiked()
    {   
        Instagram::setAccessToken(User::getAccessToken());
        $likes = Instagram::getUserLikes();


        return LevelThreeGenerators::generateLevelThreeAdvanced($likes);

    }
/*
|--------------------------------------------------------------------------
| POST /users/{id}/follow/ - Follow a specific user.
| Parameter : $id - The id of the user.
|--------------------------------------------------------------------------
|
*/
    public function postFollow($id)
    {
        Instagram::setAccessToken(User::getAccessToken());
        $data = Instagram::modifyRelationship('follow',$id);

        return Response::make(json_encode($data,JSON_PRETTY_PRINT),$data->meta->code);
       // print_r($data);

    }
/*
|--------------------------------------------------------------------------
| DELETE /users/{id}/unfollow/ - Unfollow a specific user.
| Parameter : $id - The id of the user.
|--------------------------------------------------------------------------
|
*/
    public function deleteUnfollow($id)
    {
        Instagram::setAccessToken(User::getAccessToken());
        $data = Instagram::modifyRelationship('unfollow',$id);

        Response::make(json_encode($data,JSON_PRETTY_PRINT),$data->meta->code);
        //print_r($data);

    }
} 